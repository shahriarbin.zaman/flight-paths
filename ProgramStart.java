// Shahriar Bin Zaman
// version : 1.5


import java.io.FileNotFoundException;
import java.util.Scanner;

public class ProgramStart {
    public static void main(String [] args) throws FileNotFoundException {
        // Scanner to read in user input
        Scanner readUserInput = new Scanner(System.in);
        System.out.println("Please enter the Flight data file name and requested flight data file name divided by a space, " +
                "e.g : \"FlightData.txt RequestedFlightPlans.txt\"");

        String flightDataFileName = readUserInput.next();
        String requestedFlightPlansFileName = readUserInput.next();

        // passing user input files to for the graph to be implemented and the paths shown

        ReadFile.readFlightData(flightDataFileName, requestedFlightPlansFileName);
    }
}
