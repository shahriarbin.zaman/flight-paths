// wrapper class for the edges
class OriginDestination {
    int origin;
    int destination;
    int cost;
    int time;

    public OriginDestination (int origin, int destination, int cost, int time) {
        this.origin = origin;
        this.destination = destination;
        this.cost = cost;
        this.time = time;
    }
}
