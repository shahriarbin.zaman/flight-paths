// Citation : https://www.codejava.net/java-core/collections/sorting-a-list-by-multiple-attributes-example
// Code help taken from above website to override methods to implement comparator
import java.util.Comparator;

public class StringFlights {
    String path;
    int time;
    int cost;
    int tempSortInt;

    public StringFlights(String path, int time, int cost) {
        this.path = path;
        this.time = time;
        this.cost = cost;
    }

    // time compared
    public static class StringFlightsTimeComparator implements Comparator<StringFlights> {
        @Override
        public int compare(StringFlights time1, StringFlights time2) {
            return time1.time - time2.time;
        }
    }

    // cost compared
    public static class StringFlightsCostComparator implements Comparator<StringFlights> {
        @Override
        public int compare(StringFlights cost1, StringFlights cost2) {
            return cost1.cost - cost2.cost;
        }
    }
}
